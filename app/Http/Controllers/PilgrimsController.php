<?php

namespace App\Http\Controllers;

use App\Pilgrim;
use Illuminate\Http\Request;


class PilgrimsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function Register(Request $request)
    {
        $this->validate($request, [
            'passport_id' => 'required',
            'name' => 'required',
            'nationality' => 'required',
            'password' => 'required'
        ]);

        $pilgrim = new Pilgrim;

        $pilgrim->passport_id = $request->input('passport_id');
        $pilgrim->name = $request->input('name');
        $pilgrim->nationality = $request->input('nationality');
        $pilgrim->with_campaign = $request->input('with_campaign');
        $pilgrim->is_valunteer = $request->input('is_valunteer');
        $pilgrim->password = md5($request->input('password'));
        $pilgrim->is_valunteer = $request->input('is_valunteer');
        $pilgrim->phone = $request->input('phone');
        $pilgrim->expiration_date = $request->input('expiration_date');
        $pilgrim->gender = $request->input('gender');
        $pilgrim->date_of_birth = $request->input('date_of_birth');

        $pilgrim->save();

        return response()->json(['message' => 'success', 'code' => 200]);
        
    }

    public function Login(Request $request)
    {
        $this->validate($request, [
            'passport_id' => 'required',
            'password' => 'required'
        ]);
        
        $pilgrim = Pilgrim::where('passport_id',$request->passport_id)->where('password',md5($request->password))->count();
        if($pilgrim>0)
        {
            return response()->json(['message' => 'Success', 'code' => 200]);
        }else{
            return response()->json(['message' => 'Not Found', 'code' => 204]);
        }
    }

    
}
