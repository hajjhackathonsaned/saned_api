<?php

namespace App\Http\Controllers;

use App\Pilgrim;
use Illuminate\Http\SosRequest;


class RequestsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function sendRequest(Request $request)
    {
        $this->validate($request, [
            'sos_type' => 'required',
            'pilgrim_id' => 'required'
        ]);

        $SOS = new SosRequest;

        $SOS->sos_type = $request->input('sos_type');
        $SOS->langitude = $request->input('langitude');
        $SOS->latitude = $request->input('latitude');
        $SOS->sos_media_type = $request->input('sos_media_type');
        $SOS->sos_additional_info = $request->input('sos_additional_info');
        $SOS->sos_status = $request->input('sos_status');
        $SOS->pilgrim_id = $request->input('sos_media_url');
        $SOS->expiration_date = $request->input('pilgrim_id');

        $SOS->save();

        return response()->json(['message' => 'success', 'code' => 200]);
        
    }

    public function status(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);
        
        $SOS = SosRequest::find('id',$request->id);
        return response()->json(['data' => $SOS, 'code' => 200]);
    }

    
}
